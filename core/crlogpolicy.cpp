/* basic logging policy class implementation
 *
 *
 *
 */

#include "crlogpolicy.h"

#include <cstdio>

///////////////////////////////
// CROutput2File implementation
///////////////////////////////

CROutput2File::CROutput2File()
{

}

CROutput2File::~CROutput2File()
{

}

FILE*& CROutput2File::Stream()
{
  static FILE* pStream = fopen("application.log", "a");
  return pStream;
}

void CROutput2File::Output(const std::string& msg)
{
  FILE* pStream = Stream();
  if ( !pStream ) {
      return;
  }

  fprintf(pStream, "%s", msg.c_str());
  fflush(pStream);
}






// Basic logging class implementation


#include <iostream>

template <class OutputPolicy>
CRLog<OutputPolicy>::CRLog()
{

}

template <class OutputPolicy>
CRLog<OutputPolicy>::~CRLog()
{
  // CRLog writes when it destructs

  // let's us the policy template class
  OutputPolicy::Output(m_oSS.str());
}

template <class OutputPolicy>
std::ostringstream& CRLog<OutputPolicy>::Get()
{
  return m_oSS;
}




























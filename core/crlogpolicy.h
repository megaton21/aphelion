/* basic logging policy class
 *
 *
 *
 */

#ifndef _CRLOGPOLICY_H_
#define _CRLOGPOLICY_H_

#include <sstream>

/* CROutput2File
 *
 * Policy class used as parameter for tamplated logging class (CRLog).
 * This class simply has a static FILE pointer which we flush a buffer
 * to.
 *
 * NB: Output is the required functon when this class is used as a template
 * parameter.
 *
 */
class CROutput2File 
{
 public:
  // constructor
  CROutput2File();

  // destructor
  ~CROutput2File();

  // copy constructor - disabled
  CROutput2File(const CROutput2File& oCopy) = delete;

  // assignment operator - disabled
  CROutput2File& operator=(const CROutput2File& rhs) = delete;

  // move constructor - disabled
  CROutput2File(CROutput2File&& oCopy) = delete;

  // move assignment operator - disabled
  CROutput2File& operator=(CROutput2File&& rhs) = delete;

  // function to get the pointer (by reference) for the stream
  static FILE*& Stream();

  // main output function that this policy encapsulates
  static void Output(const std::string& msg);

};

#endif // _CRLOGPOLICY_H_








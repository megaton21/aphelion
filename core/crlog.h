
/* basic logging
 *
 *
 *
 */

#ifndef _CRLOG_H_
#define _CRLOG_H_

#include "crlogpolicy.h"

#include <sstream>

/* CRLog
 *
 * Templated logging class. This is a basic logging class that takes a template
 * parameter. The template parameter is a policy class that represents the logging
 * action.
 */
template <class OutputPolicy>
class CRLog
{
 public:
  // constructor
  CRLog();

  // destructor
  ~CRLog();

  // copy constructor - disabled
  CRLog(const CRLog& oCopy) = delete;

  // assignment operator - disabled
  CRLog& operator=(const CRLog& rhs) = delete;

  // move constructor - disabled
  CRLog(CRLog&& oCopy) = delete;

  // move assignment operator - disabled
  CRLog& operator=(CRLog&& rhs) = delete;

  // get the string stream
  std::ostringstream& Get();

  // string stream to hold logging info
  std::ostringstream m_oSS;

};



#include "crlog.hpp"

// creating a class with a specific policy class
typedef CRLog<CROutput2File> CRLogFile;

#endif // _CRLOG_H_













